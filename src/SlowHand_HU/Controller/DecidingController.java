package SlowHand_HU.Controller;

import SlowHand_HU.AI.CardManager;
import SlowHand_HU.AI.Statistics;
import SlowHand_HU.AI.StyleList;
import SlowHand_HU.Debug.Debug_Logger;

public class DecidingController extends BaseController {
    private Statistics statistics;

    public DecidingController(CardManager cardManager) {
        super(cardManager);
        Debug_Logger.getInstance().addLog(String.format("%s Controller létrehozva",this.toString()),5);
    }

    public boolean Start() {
        statistics = new Statistics(cardManager);
        StyleList styleList = new StyleList(statistics);
        return styleList.makeDecision();
    }

}
