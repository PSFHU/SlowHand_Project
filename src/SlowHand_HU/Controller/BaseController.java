package SlowHand_HU.Controller;

import SlowHand_HU.AI.CardManager;

public abstract class BaseController {
    protected CardManager cardManager;

    public BaseController(CardManager cardManager) {
        this.cardManager = cardManager;
    }
}
