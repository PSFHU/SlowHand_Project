package SlowHand_HU.Controller;

import SlowHand_HU.AI.CardManager;
import SlowHand_HU.Debug.Debug_Logger;
import hu.pte.poker.framework.entity.Player;

public class AiController extends BaseController implements Player {


    public AiController(CardManager cardManager) {
        super(cardManager);
        Debug_Logger.getInstance().addLog(String.format("%s Controller létrehozva",this.toString()),5);
    }

    @Override
    public void deal(String s, int i) {
        cardManager.setFirstRound(true);
        cardManager.szovegLapokForditasaJatekos(s);
    }

    @Override
    public boolean areYouIn() {
        return new DecidingController(cardManager).Start();
    }

    @Override
    public void flop(String s, int i) {
        cardManager.szovegLapokForditasaAsztal(s);
        cardManager.setFirstRound(false);
    }

    @Override
    public void turn(String s, int i) {
        cardManager.szovegLapokForditasaAsztal(s);
    }

    @Override
    public void river(String s, int i) {
        cardManager.szovegLapokForditasaAsztal(s);
    }

    @Override
    public void result(boolean b, int i) {
    }

}
