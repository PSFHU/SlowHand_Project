package SlowHand_HU.Controller;

import SlowHand_HU.ConsoleColors;
import SlowHand_HU.Debug.Debug_Logger;
import hu.pte.poker.framework.entity.Player;

import java.util.Scanner;

public class ConsolePlayerController implements Player {

    private boolean elsoKor;
    private Scanner scanner;
    private String jatekosLapjai;
    private int jatekosokSzama;
    private String asztalonLapok = "";

    public ConsolePlayerController(Scanner scanner) {
        this.scanner = scanner;
        Debug_Logger.getInstance().addLog(String.format("%s Controller létrehozva",this.toString()),5);
    }

    @Override
    public void deal(String s, int i) {
        elsoKor = true;
        kiirAllapot(s,i,elsoKor);
        elsoKor = false;
    }

    @Override
    public boolean areYouIn() {
        return beviszBentmaradas();
    }

    @Override
    public void flop(String s, int i) {
        kiirAllapot(s,i,elsoKor);
    }

    @Override
    public void turn(String s, int i) {
        kiirAllapot(s,i,elsoKor);
    }

    @Override
    public void river(String s, int i) {
        kiirAllapot(s,i,elsoKor);
    }

    @Override
    public void result(boolean b, int i) {
        kiirEredmenyek(b,i);
    }

    private boolean beviszBentmaradas() {
        String choice;
        do {
            choice = "";
            System.out.print("Most már tudja, hogy milyen lapjai vannak. Kérem döntsön!" +
                    "(B - Benne maradok a játékban, D - Dobom a lapjaimat): ");
            while (choice.isEmpty()) {
                choice = scanner.nextLine();
            }

            choice = choice.toUpperCase();
            switch (choice) {
                case "B":
                    return true;
                case "D":
                    System.out.println("Kiesett a játékból.");
                    return false;
            }
        } while (choice.compareTo("B") != 0 || choice.compareTo("D") != 0);
        return true;
    }

    private void kiirAllapot(String cards, int players, boolean firstRound){

        if(firstRound){
            System.out.println("Az Ön lapjai: " + cards);
            jatekosLapjai = cards;
            jatekosokSzama = players;
        }
        else if (asztalonLapok.equals("")) {
            System.out.println(ConsoleColors.GREEN_UNDERLINED + "[" + jatekosokSzama + "/" + players + " aktív játékos]" + ConsoleColors.RESET);
            System.out.println("Az Ön lapjai: " + jatekosLapjai);
            System.out.println("Asztalon lévő lapok: " + cards);
            asztalonLapok = cards;
        }

        else {
            System.out.println(ConsoleColors.GREEN_UNDERLINED + "[" + jatekosokSzama + "/" + players + " aktív játékos]" + ConsoleColors.RESET);
            System.out.println("Az Ön lapjai: " + jatekosLapjai);
            System.out.print("Asztalon lévő lapok: " + asztalonLapok + ",");
            System.out.println(ConsoleColors.RED_BOLD + cards + ConsoleColors.RESET);
            asztalonLapok = asztalonLapok + "," + cards;
        }
    }

    private void kiirEredmenyek(boolean b, int i) {
        if (b){
            System.out.println("Gratulálunk! Ön nyert!");
            System.out.println("A nyereménye: "+i+" forint.");
        }
        else {System.out.println("Sajnáljuk! Ön veszített!");}
    }


}

