package SlowHand_HU.Debug;

import SlowHand_HU.ConsoleColors;

import java.util.ArrayList;
import java.util.Date;

public class Debug_Logger {
      /* Logolási szintek
    1. Szint Csak a nagyobb hibák kerüljenek kiírásra
    2. Szint A néha felmerülő hibáké
    3. Szint Logikai folyamatok szemléltetése
    4. Szint Metódus meghívások kiírása
    5. Szint Az osztály készítések kiírása
     */

    private static Debug_Logger ourInstance = new Debug_Logger();

    public static Debug_Logger getInstance() {
        return ourInstance;
    }

    private Debug_Logger() {
        this.logs = new ArrayList<>();
        this.displayLevel = 0;
        this.addLog(String.format("%s osztály létrehozva",this.toString()),5);
    }

    private ArrayList<Log> logs;
    private int displayLevel;

    public int getDisplayLevel() {
        return displayLevel;
    }

    public void setDisplayLevel(int displayLevel) {
        if (displayLevel < 6)
            this.displayLevel = displayLevel;
    }

    public void addLog(String content, int level){
        Log log = new Log(content,level,new Date());
        logs.add(log);
        displayLog(log);
    }

    private void displayLog(Log log){
        if (log.getLevel() <= displayLevel) {
            switch (log.getLevel()){
                case 1:
                    System.out.println(ConsoleColors.RED+log.toString()+ConsoleColors.RESET);
                    break;
                case 2:
                    System.out.println(ConsoleColors.YELLOW+log.toString()+ConsoleColors.RESET);
                    break;
                case 3:
                    System.out.println(ConsoleColors.CYAN+log.toString()+ConsoleColors.RESET);
                    break;
                case 4:
                    System.out.println(ConsoleColors.PURPLE+log.toString()+ConsoleColors.RESET);
                    break;
                case 5:
                    System.out.println(ConsoleColors.GREEN+log.toString()+ConsoleColors.RESET);
                    break;
            }
        }
    }

    public void displayAllLog(){
        for (Log log : logs){
            System.out.println(log.toString());
        }
    }
}
