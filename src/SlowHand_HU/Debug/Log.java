package SlowHand_HU.Debug;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
    private String content;
    private int level;
    private Date createTime;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Log(String content, int level, Date createTime) {
        this.content = content;
        this.level = level;
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return String.format("[%s] (%d. szintű) - %s",dateFormat.format(getCreateTime()),getLevel(),getContent());
    }
}
