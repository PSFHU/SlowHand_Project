package SlowHand_HU.AI;

import SlowHand_HU.Debug.Debug_Logger;
import hu.pte.poker.framework.entity.Card;

import java.util.ArrayList;

public class CardManager {
    private boolean firstRound;

    private ArrayList<Card> lapokAsztal;
    private ArrayList<Card> lapokJatekos;

    public ArrayList<Card> getLapokJatekos() {
        return lapokJatekos;
    }
    public ArrayList<Card> getLapokAsztal() {
        return lapokAsztal;
    }
    public ArrayList<Card> getLapokOssz(){
        ArrayList<Card> osszList = new ArrayList<Card>(lapokAsztal);
        osszList.addAll(lapokJatekos);
        return osszList;
    }

    public CardManager() {
        this.lapokAsztal = new ArrayList<>();
        this.lapokJatekos = new ArrayList<>();
        firstRound = true;
        Debug_Logger.getInstance().addLog(String.format("%s osztály létrehozva",this.toString()),5);
    }

    public boolean isFirstRound() {
        return firstRound;
    }

    public void setFirstRound(boolean firstRound) {
        this.firstRound = firstRound;
    }

    // String és Enum közti fordításért felelős metódusok
    // Eleje
    static enum nyersKodSzoveg {
        t("N10"),
        j("JACK"),
        q("QUEEN"),
        k("KING"),
        a("ACE"),
        c("CLUB"),
        d("DIAMOND"),
        h("HEART"),
        s("SPADE");

        private String code;

        private nyersKodSzoveg(String code) {
            this.code = code;
        }
    }

    private String convertStringtoEnum(String lap){
        String teszt = "";
        try{
            // Ha a valueOf-hoz hozzáírod hogy .code akkor a baloldalt viszi vissza Stringként
            teszt = nyersKodSzoveg.valueOf(lap).code;
        }catch (Exception e){
            teszt = "N" + lap;
        }

        return teszt;
    }
    // Vége


    public void szovegLapokForditasaAsztal(String szovegLapok){

        String[] kulonLapok = formaztottLapok(szovegLapok);

        Card tmpCard;
        for (int i = 0; i < kulonLapok.length; i++) {
            tmpCard = new Card(Card.Color.valueOf(convertStringtoEnum(kulonLapok[i].charAt(0)+"")),
                    Card.Name.valueOf(convertStringtoEnum(kulonLapok[i].charAt(1)+"")));
            lapokAsztal.add(tmpCard);
        }

    }
    public void szovegLapokForditasaJatekos(String szovegLapok){

        String[] kulonLapok = formaztottLapok(szovegLapok);

        Card tmpCard;
        for (int i = 0; i < kulonLapok.length; i++) {

           tmpCard = kartyaKeszito(kulonLapok[i].charAt(0)+"",kulonLapok[i].charAt(1)+"");
           
           lapokJatekos.add(tmpCard);
        }

    }

    private String[] formaztottLapok(String szovegLapok){
        return szovegLapok.split(",");
    }

    private Card kartyaKeszito(String szin,String ertek){
        return new Card(Card.Color.valueOf(convertStringtoEnum(szin)),
                Card.Name.valueOf(convertStringtoEnum(ertek)));
    }

}
