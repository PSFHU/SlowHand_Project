package SlowHand_HU.AI;

import SlowHand_HU.Debug.Debug_Logger;
import hu.pte.poker.framework.entity.Card;
import hu.pte.poker.framework.service.HandService;

import java.util.ArrayList;
import java.util.Random;

public class Statistics {
    private CardManager cm;
    private HandService handService;


    public Statistics(CardManager cm) {
        this.cm = cm;
        handService = new HandService();
        Debug_Logger.getInstance().addLog(String.format("%s osztály létrehozva",this.toString()),5);
    }

    public double getCalculatedSzazalek(){
        double value;

        int cardDistanceRatio = 50;
        int cardDistanceSplit = 12;

        int handValueRatio = 30;
        int handValueSplit = 14;

        if (2 < cm.getRound()) {
            value = ((double) (handValueRatio / handValueSplit) * calculateValueOfHand() +  (cardDistanceRatio - (double) ((cardDistanceRatio / cardDistanceSplit) * getHandCardDistance(cm.getLapokJatekos())) ) );
            Debug_Logger.getInstance().addLog("Kéz Értéke: " + calculateValueOfHand(), 3);
        }
        else {
            value = (( 100 - (double) (100/12) * getHandCardDistance(cm.getLapokJatekos())));
        }
        Debug_Logger.getInstance().addLog("Érték: " + value,3);
        Debug_Logger.getInstance().addLog("Össz: " + cm.getLapokOssz(),3);
        Debug_Logger.getInstance().addLog("Játékos: " + cm.getLapokJatekos(),3);
        Debug_Logger.getInstance().addLog("Asztal: " + cm.getLapokAsztal(),3);

        return value;
    }

    // Ez a metódus 0-12 közötti értéket vihet vissza
    private int getHandCardDistance(ArrayList<Card> cards){
        int first = calculateValueOfCard(cards.get(0).getName().getCode());
        int second = calculateValueOfCard(cards.get(1).getName().getCode());
        if (first > second)
            return first - second;
        else if (first < second)
            return second - first;
        else
            return 10;
    }

    // Ez a metódus 0-14 közötti értéket vihet vissza
    private int calculateValueOfHand(){
        int value = 0;

        for (Card card : cm.getLapokAsztal()){
            for (Card cardPlayer : cm.getLapokJatekos()){
                if (card.getColor() == cardPlayer.getColor()){
                    value++;
                }
                if (card.getName() == cardPlayer.getName()){
                    value++;
                }
            }
        }

        return value;
    }

    private int calculateValueOfCard(String code){
        int value = 0;
        try {
            value = Integer.parseInt(code);
        }catch (Exception e){
            switch (code){
                case "t":
                    value = 10;
                break;
                case "j":
                    value = 11;
                break;
                case "q":
                    value = 12;
                break;
                case "k":
                    value = 13;
                break;
                case "a":
                    value = 14;
                break;
            }
        }
        return value;
    }

    private int calculateValueOfCard(String code){
        int value = 0;
        try {
            value = Integer.parseInt(code);
        }catch (Exception e){
            switch (code){
                case "t":
                    value = 10;
                break;
                case "j":
                    value = 11;
                break;
                case "q":
                    value = 12;
                break;
                case "k":
                    value = 13;
                break;
                case "a":
                    value = 14;
                break;
            }
        }
        return value;
    }

    // Ez a metódus a legegyszerűbb és teljesen véletlen
    public int getRandomSzazalek(){
        Random rnd = new Random();
        return rnd.nextInt(100 + 1);
    }
}


