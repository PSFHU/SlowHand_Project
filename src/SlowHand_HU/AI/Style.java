package SlowHand_HU.AI;

public class Style{
    private int max;
    private int min;

    public Style(int max, int min) {
        this.max = max;
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }
}
