package SlowHand_HU.AI;

import SlowHand_HU.Debug.Debug_Logger;

import java.util.ArrayList;
import java.util.Random;

public class StyleList {
    private ArrayList<Style> styleArrayList;
    private Statistics statistics;
    private Random rnd = new Random();

    private int currentStyle = 0;

    public StyleList(Statistics statistics) {
        this.statistics = statistics;
        styleArrayList = new ArrayList<>();
        createStyles();
        Debug_Logger.getInstance().addLog(String.format("%s osztály létrehozva",this.toString()),5);
    }

    private void createStyles(){
        styleArrayList.add(new Style(100,60));
        styleArrayList.add(new Style(100,50));
        styleArrayList.add(new Style(100,30));
    }

    public boolean makeDecision(){
        setStyle();
        double calculated = statistics.getCalculatedSzazalek();     // itt valamiért gyakori a dobás
        if (getMax() >= calculated && calculated >= getMin() ){
            return true;
        }else
            return false;                                            // ez a sor csak a debug miatt true!
    }

    private void setStyle(){
        currentStyle = rnd.nextInt(styleArrayList.size());
        if (currentStyle == 0){
            Debug_Logger.getInstance().addLog("[AI - megfontolt döntés]",3);
        } else if (currentStyle == 1) {
            Debug_Logger.getInstance().addLog("[AI - normál döntés]",3);
        } else {
            Debug_Logger.getInstance().addLog("[AI - bevállalós döntés]",3);
        }
    }


    private int getMax(){
        return styleArrayList.get(currentStyle).getMax();
    }

    private int getMin(){
        return styleArrayList.get(currentStyle).getMin();
    }

}
