package SlowHand_HU;

public class ConsoleColors {

    // Sima színek
    public static final String RESET = "\033[0m";  // Text Reset
    public static final String YELLOW = "\033[0;33m";  // YELLOW
    public static final String RED = "\033[0;31m";     // RED
    public static final String PURPLE = "\033[0;35m";  // PURPLE
    public static final String GREEN = "\033[0;32m";   // GREEN
    public static final String CYAN = "\033[0;36m";    // CYAN

    // Bold
    public static final String RED_BOLD = "\033[1;31m";    // RED

    // Underlined
    public static final String GREEN_UNDERLINED = "\033[4;32m";  // GREEN
}
