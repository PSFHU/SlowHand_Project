package SlowHand_HU;

import SlowHand_HU.AI.CardManager;
import SlowHand_HU.Controller.AiController;
import SlowHand_HU.Controller.ConsolePlayerController;
import SlowHand_HU.Debug.Debug_Logger;
import hu.pte.poker.framework.entity.Party;

import java.util.Scanner;

public class Main{

    public static void main(String[] args) {
        // Golbálisan használt osztályok
        Debug_Logger.getInstance().setDisplayLevel(3);
        Scanner scanner = new Scanner(System.in);
        Party meccs = new Party();

        // Kontrollerek (Játékosok példányosítása)
        AiController aiController = new AiController(new CardManager());
        AiController aiController1 = new AiController(new CardManager());
        AiController aiController2 = new AiController(new CardManager());
        ConsolePlayerController roland =new ConsolePlayerController(scanner);

        // Party irányításával kapcsolatos műveletek
        meccs.addPlayers(roland, aiController, aiController1, aiController2 );
        meccs.play();

    }
}
