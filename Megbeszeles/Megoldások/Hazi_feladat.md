HÁZI FELADAT

1. JAR fájlt hozzá kell adni, függőségként 
2. Csinálni kell játékosokat (2db-ot) - Consol - automatikus 
3. Készítsen egy példányt a Partyból
4. Adja hozzá a játkosokat (mindkét félét)
5. Hívja meg a Play metódust
6. Ellenőrizze a player belső állapotait debuggolással

KÁRTYA

1. Színek
-c(clubs) -h(hearts) -d(diamonds) -s(spades)
2. Értékek
-2-3-4-5-6-7-8-9-t-j-q-k-a

PLAYER INTERFACE
1. void deal(Stringcards, int numberOfPlayers) 
	cards: két kártyát tartalmaz, ","-vel elválasztva
	numberOfPlayers: azoknak a játékosoknak a száma, akik még mindig játékban vannak
2. boolean areYouIn() 
	ha nem, akkor eldobja
3. void flop(Stringcards, int numberOfPlayers)
	3 kártyát tartalmaz
4. void turn(Stringcards, int numberOfPlayers)
	1 kártyát tartalmaz
5. void river(Stringcards, int numberOfPlayers)