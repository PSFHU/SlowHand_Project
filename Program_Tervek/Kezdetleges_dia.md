#Kezdetleges program algoritmus
>>>
    Jelenleg csak a kártyákra fókuszálva írtam meg az algoritmust.
    Ezt a következő megbeszélésen megvitathatjuk.
    Ez nem csak egy örökélési algoritmus! (Az öröklés jelőlője a ->)
>>>
```mermaid
graph TD;

A[Játékos]---|Műveletek void deal, bool areYouIn, void flop, turn, void river, void result,|E[Döntő];
E---|Kártyák használatára|B[Kártya kezelő];

E---Statisztika;
E---Pénzkezelő;

B---C[Kártya fordító];
B---D[Kártya tároló];
B---K[Kártyák];

K-->Asztal;
K-->Kéz;
K-->Logikai;
```
