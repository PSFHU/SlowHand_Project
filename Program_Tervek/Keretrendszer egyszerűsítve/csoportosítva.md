**CARD OSZTÁLY**    
-Serializable leszármazottja

VÁLTOZÓK

-serialVersion =1L

-Card.color color

-Card.name name

ENUMOK

-static enum Name

	 N2("2"),N3("3"), N4("4"),N5("5"),N6("6"),N7("7"),N8("8"),N9("9"),
	 N10("t"),JACK("j"),QUEEN("q"),KING("k"),ACE("a");
	 
	 private String code;
        private Name(String code) 
            this.code = code;
        public String getCode() 
            return this.code;
        public void setCode(String code) 
            this.code = code;
-static enum Color

{
        CLUB("c"),
        DIAMOND("d"),
        HEART("h"),
        SPADE("s");
        private String code;
        private Color(String code) 
           this.code = code;
        public String getCode() 
            return this.code;
        public void setCode(String code)
            this.code = code;
      
KONSTRUKTOR

-color

-name

METÓDUSOK RÖVIDEN

-String toString()

-boolean equals(String color, String name)

-boolean equals(Object obj)

-Card.Color getColor()

-Card.Name getName()

-int getOrder()

METÓDUSOK BŐVEBBEN

-String toString()

	return "" + this.color.code + "" + this.name.code + ""
-boolean equals(String color, String name)

        return this.color.name().equals(color) && this.name.name().equals(name);
-boolean equals(Object obj)

	if (obj instanceof Card) {
            Card card = (Card)obj;
            return this.equals(card.color.name(), card.name.name());
        } else {
            return super.equals(obj);}
            
GETTER/SETTER
-Card.Color getColor()

	return this.color;
-Card.Name getName()

	return this.name;
****-int getOrder()	

	return this.name.ordinal();
	
	
	
	


**DECK OSZTÁLY    DECK OSZTÁLY    DECK OSZTÁLY    DECK OSZTÁLY    DECK OSZTÁLY    DECK OSZTÁLY    DECK OSZTÁLY    DECK OSZTÁLY**

-Serializable leszármazottja

VÁLTOZÓK

-static final long serialVersionUID = 1L

-List<Card> cards = new ArrayList()

-int index

KONSTRUKTOR

        Color[] colors = Color.values();
        Name[] names = Name.values();
        Color[] var4 = colors;
        int var5 = colors.length;
  	 for(int var6 = 0; var6 < var5; ++var6) {
            Color color = var4[var6];
            Name[] var8 = names;
            int var9 = names.length;
            for(int var10 = 0; var10 < var9; ++var10) {
                Name name = var8[var10];
                Card card = new Card(color, name);
                this.cards.add(card);}

METÓDUSOK RÖVIDEN

Deck shuffle() 

String toString()

-public List<Card> getCards(int num)
 
-List<Card> getCards()

METÓDUSOK BŐVEBBEN

-Deck shuffle() 

	this.index = 0;
        Collections.shuffle(this.cards);
        return this;
-String toString()

	StringBuilder sb = new StringBuilder();
        Card card;
        for(Iterator var2 = this.cards.iterator(); var2.hasNext(); sb.append(card.toString())) {
            card = (Card)var2.next();
            if (sb.length() > 0) {
                sb.append(",");
            } else {
                sb.append("[");}}
        sb.append("]");
        return sb.toString();
-public List<Card> getCards(int num) 

	List<Card> result = new ArrayList();

        for(int limit = this.index + num; this.index < limit; ++this.index) {
            result.add(this.cards.get(this.index));}
        return result;
GETTER/SETTER

-List<Card> getCards()

	return this.cards
	
	
	
	
	

**HAND OSZTÁLY    HAND OSZTÁLY    HAND OSZTÁLY    HAND OSZTÁLY    HAND OSZTÁLY    HAND OSZTÁLY    HAND OSZTÁLY    HAND OSZTÁLY**

 - implementálja a Comparable<Hand> -et

VÁLTOZÓK

-HandService handService

-HAND_VALUE value

-List<Card> cards

-PlayerProxy playerProxy

KONSTRUKTOR

-public Hand(List<Card> cards, PlayerProxy playerProxy) 

	this.playerProxy = playerProxy;
        this.handService = HandService.getInstance();
        this.value = this.handService.getValue(cards);
        this.cards = cards;

METÓDUSOK RÖVIDEN

-int compareTo(Hand other)

-String toString()

-HAND_VALUE getValue()

-List<Card> getCards()

-PlayerProxy getPlayerProxy()

-void setPlayerProxy(PlayerProxy playerProxy)


METÓDUSOK BŐVEBBEN

-int compareTo(Hand other)

	return this.handService.compare(this, other)
-String toString()

	"[" + this.getClass().getSimpleName() + " - cards: " + this.cards + " - value: " + this.value + "]"
GETTER/SETTER

-HAND_VALUE getValue()

	return this.value
-List<Card> getCards()

	return this.cards;
-PlayerProxy getPlayerProxy()

	return this.playerProxy;
-void setPlayerProxy(PlayerProxy playerProxy)

        this.playerProxy = playerProxy;







**PARTY CLASS PARTY CLASS PARTY CLASS PARTY CLASS PARTY CLASS PARTY CLASS PARTY CLASS PARTY CLASS PARTY CLASS PARTY CLASS PARTY CLASS**

VÁLTOZÓK

-List<PlayerProxy> players;

-Deck deck = new Deck();

-List<Card> sharedCards;

-int pot;

KONSTRUKTOR

-üres

METÓDUSOK RÖVIDEN

-void addPlayers(Player... players)

-void play()

-void askPlayersIfFold()

-void deal()

-void initParty()

-void flop()

-void turn()

-void river()

-void result()

-List<Hand> getPlayersBestHand(List<PlayerProxy> playersInParty)

-Hand getPlayerBestHand(PlayerProxy player)

-void fillPlayerHands(Set<Hand> hands, List<Card> cards, PlayerProxy player)

-void addNewHand(Set<Hand> hands, Set<Card> set, PlayerProxy player)

-void fillSet(List<Card> cards, Set<Card> set, int i1, int i2, int i3, int i4, int i5) {

-String getNewSharedCards(int num)

-private List<PlayerProxy> getPlayersInParty()

METÓDUSOK BŐVEBBEN
-void addPlayers(Player... players)

	this.players = new ArrayList();
        Player[] var2 = players;
        int var3 = players.length;
        for(int var4 = 0; var4 < var3; ++var4) {
            Player player = var2[var4];
            this.players.add(new PlayerProxy(player));
- public void play()

        this.deal();
        this.askPlayersIfFold();
        this.flop();
        this.askPlayersIfFold();
        this.turn();
        this.askPlayersIfFold();
        this.river();
        this.askPlayersIfFold();
        this.result();
-void askPlayersIfFold()

        List<PlayerProxy> playersInParty = this.getPlayersInParty();
        Iterator var2 = playersInParty.iterator();
        while(var2.hasNext()) {
            PlayerProxy player = (PlayerProxy)var2.next();
            if (player.areYouIn()) {
                ++this.pot; } else {
                player.setInParty(false);
-void deal()

        this.initParty();
        List<PlayerProxy> playersInParty = this.getPlayersInParty();
        Iterator var2 = playersInParty.iterator();
        while(var2.hasNext()) {
            PlayerProxy player = (PlayerProxy)var2.next();
            List<Card> cards = this.deck.getCards(2);
            player.setHandCards(cards);
            String carsdStr = StringUtil.join(cards, ",");
            player.deal(carsdStr, playersInParty.size());
-void initParty()

        this.deck.shuffle();
        this.sharedCards = new ArrayList();
        this.pot = 0;
        Iterator var1 = this.players.iterator();
        while(var1.hasNext()) {
            PlayerProxy playerProxy = (PlayerProxy)var1.next();
            playerProxy.setInParty(true);
-void flop() 

	        String newCardsStr = this.getNewSharedCards(3);
        List<PlayerProxy> playersInParty = this.getPlayersInParty();
        Iterator var3 = playersInParty.iterator();
        while(var3.hasNext()) {
            PlayerProxy player = (PlayerProxy)var3.next();
            player.flop(newCardsStr, playersInParty.size());
-private void turn()

	String newCardsStr = this.getNewSharedCards(1);
        List<PlayerProxy> playersInParty = this.getPlayersInParty();
        Iterator var3 = playersInParty.iterator();
        while(var3.hasNext()) {
            PlayerProxy player = (PlayerProxy)var3.next();
            player.turn(newCardsStr, playersInParty.size())	
-private void river()

        String newCardsStr = this.getNewSharedCards(1);
        List<PlayerProxy> playersInParty = this.getPlayersInParty();
        Iterator var3 = playersInParty.iterator();
        while(var3.hasNext()) {
            PlayerProxy player = (PlayerProxy)var3.next();
            player.river(newCardsStr, playersInParty.size());
-void result()

	List<PlayerProxy> playersInParty = this.getPlayersInParty();
        List<Hand> hands = this.getPlayersBestHand(playersInParty);
        Collections.sort(hands, HandService.getInstance());
        Hand winnerHand = (Hand)hands.get(0);
        PlayerProxy winner = winnerHand.getPlayerProxy();
        Iterator var5 = playersInParty.iterator();
        while(var5.hasNext()) {
            PlayerProxy player = (PlayerProxy)var5.next();
            player.result(winner.equals(player), this.pot);
        System.out.println("The winner hand is " + winnerHand + " ");
-private List<Hand> getPlayersBestHand(List<PlayerProxy> playersInParty)

	        List<Hand> hands = new ArrayList();
        Iterator var3 = playersInParty.iterator();
        while(var3.hasNext()) {
            PlayerProxy player = (PlayerProxy)var3.next();
            Hand hand = this.getPlayerBestHand(player);
            hands.add(hand);}
        return hands;
-Hand getPlayerBestHand(PlayerProxy player)

        TreeSet<Hand> hands = new TreeSet();
        List<Card> cards = new ArrayList();
        cards.addAll(this.sharedCards);
        cards.addAll(player.getHandCards());
        this.fillPlayerHands(hands, cards, player);
        Hand bestHand = (Hand)hands.last();
        return bestHand;
-void fillPlayerHands(Set<Hand> hands, List<Card> cards, PlayerProxy player)

	        Set<Card> set = new HashSet();
        for(int i1 = 0; i1 < 7; ++i1) {
            for(int i2 = 0; i2 < 7; ++i2) {
                for(int i3 = 0; i3 < 7; ++i3) {
                    for(int i4 = 0; i4 < 7; ++i4) {
                        for(int i5 = 0; i5 < 7; ++i5) {
                            this.fillSet(cards, set, i1, i2, i3, i4, i5);
                            if (set.size() == 5) {
                                this.addNewHand(hands, set, player);
-void addNewHand(Set<Hand> hands, Set<Card> set, PlayerProxy player)

	List<Card> list = new ArrayList(set);
        Hand hand = new Hand(list, player);
        hands.add(hand);
-void fillSet(List<Card> cards, Set<Card> set, int i1, int i2, int i3, int i4, int i5)

        set.clear();
        set.add(cards.get(i1));
        set.add(cards.get(i2));
        set.add(cards.get(i3));
        set.add(cards.get(i4));
        set.add(cards.get(i5));
-String getNewSharedCards(int num)

        List<Card> newCards = this.deck.getCards(num);
        this.sharedCards.addAll(newCards);
        String newCardsStr = StringUtil.join(newCards, ",");
        return newCardsStr;
-List<PlayerProxy> getPlayersInParty()

        return (List)this.players.stream().filter((table) -> {
            return table.isInParty();
        }).collect(Collectors.toList());





**PLAYERPROXYCLASS    PLAYERPROXYCLASS    PLAYERPROXYCLASS    PLAYERPROXYCLASS    PLAYERPROXYCLASS    PLAYERPROXYCLASS    PLAYERPROXYCLASS**

-implementálja a Player interface-t

VÁLTOZÓK

-Player player;

-List<Card> handCards;

-boolean inParty;

KONSTRUKTOR

 public PlayerProxy(Player player) 
 {
        this.player = player;}

METÓDUSOK RÖVIDEN

-void deal(String cards, int numberOfPlayers) 

-boolean areYouIn()

-void flop(String cards, int numberOfPlayers)

-void turn(String cards, int numberOfPlayers)

-void river(String cards, int numberOfPlayers)

-void result(boolean youWon, int numberOfBetsWon)

-List<Card> getHandCards()

-void setHandCards(List<Card> handCards)

-boolean isInParty()

-void setInParty(boolean inParty)

-Player getPlayer()

-void setPlayer(Player player)

METÓDUSOK BŐVEBBEN
-void deal(String cards, int numberOfPlayers) 

	this.player.deal(cards, numberOfPlayers);
-boolean areYouIn()

	 return this.player.areYouIn();
-void flop(String cards, int numberOfPlayers)

	this.player.flop(cards, numberOfPlayers)
-void turn(String cards, int numberOfPlayers)

	this.player.turn(cards, numberOfPlayers)
-void river(String cards, int numberOfPlayers)

	this.player.river(cards, numberOfPlayers)
-void result(boolean youWon, int numberOfBetsWon)

	this.player.result(youWon, numberOfBetsWon) 
-boolean isInParty()

	return this.inParty;
	
GETTER/SETTER

-void setInParty(boolean inParty)

	this.inParty = inParty;
-Player getPlayer()

	return this.player;
-void setPlayer(Player player)

	this.player = player;
-List<Card> getHandCards()

	return this.handCards;
-void setHandCards(List<Card> handCards)

	this.handCards = handCards
	
	
	



PLAYER INTERFACE    PLAYER INTERFACE    PLAYER INTERFACE    PLAYER INTERFACE    PLAYER INTERFACE    PLAYER INTERFACE    PLAYER INTERFACE    
-interface

METÓDUSOK RÖVIDEN
-void deal(String var1, int var2);
-    boolean areYouIn();
-    void flop(String var1, int var2);-
-    void turn(String var1, int var2);
-    void river(String var1, int var2);
-    void result(boolean var1, int var2);






