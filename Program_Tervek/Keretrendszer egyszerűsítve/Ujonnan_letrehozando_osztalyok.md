
>>>
   Az ábrában található nyilak nem öröklést jelképeznek.
>>>

```mermaid
graph TD;
A[Kontroller osztály] -->B[Kártya fordító osztály]
A --> C[Stílus osztály]
C -->D[Statisztika osztály]
```
